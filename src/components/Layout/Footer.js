import React from "react"

const Footer = () => {
  return (
    <footer className="pv4 ph3 ph5-m ph6-l mid-gray">
      <small className="f6 db tc">
        © 2020 <b className="ttu">Stefan Schuchlenz</b>. Made with <a href="https://www.gatsbyjs.org" target="_BLANK" rel="noopener noreferrer">GatsbyJS</a>.
      </small>
      <div className="tc mt3">
        <p className="f7">This is just a sample site for demonstration purposes.</p>
      </div>
    </footer>
  )
}

export default Footer
