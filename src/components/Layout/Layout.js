import React, { Component } from 'react';
import Header from './Header'
import Footer from './Footer'

import 'tachyons/css/tachyons.css'
import './Layout.sass'

class Layout extends Component {
  constructor(props) {
    super(props);
    // this.state = {  }
  }
  render() { 
    return (
      <div className={"pa0 mw7 sans-serif " + this.props.className} style={{margin: 'auto'}}>
        <Header/>
        {this.props.children}
        <Footer/>
      </div>
    );
  }
}
 
export default Layout;