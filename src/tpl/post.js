import React from "react"
import Layout from "../components/Layout/Layout.js"
import { graphql, Link } from "gatsby"

import htmlHelpers from "../helpers/htmlHelpers"

const Post = props => {
  console.log(props)

  return (
    <Layout className="pa3">
      <h1>{props.data.markdownRemark.frontmatter.title}</h1>
      <div
        className="post-content"
        dangerouslySetInnerHTML={htmlHelpers.innerHTML(
          props.data.markdownRemark.html
        )}
      ></div>
      <hr />
      <div className="post-meta">
        <strong>Author: </strong>
        {props.data.markdownRemark.frontmatter.author}
        &nbsp;|&nbsp;
        <strong>Date: </strong>
        {props.data.markdownRemark.frontmatter.date}
        &nbsp;|&nbsp;
        <Link to="/">back</Link>
      </div>
    </Layout>
  )
}

export const query = graphql`
  query($slug: String!) {
    markdownRemark(frontmatter: { slug: { eq: $slug } }) {
      frontmatter {
        slug
        author
        date
        tags
        title
      }
      html
    }
  }
`

export default Post
