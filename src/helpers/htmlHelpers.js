// Various small helper funcs go here
let htmlHelpers = {
  // Prepare html content to use as dangerouslySetInnerHTML
  innerHTML: (html) => {
    return {__html: html};
  }
}

module.exports = htmlHelpers;