import React, { Component } from 'react';
import Layout from '../components/Layout/Layout'
import { graphql, Link } from 'gatsby';

export const query = graphql`
  query loadPostList {
    allMarkdownRemark(sort: {fields: frontmatter___date, order: DESC}) {
      edges {
        node {
          frontmatter {
            slug
            author
            date
            tags
            title
          }
          excerpt
        }
      }
    }
  }
`
export const listPosts = (posts) => {
  console.log(posts)
  return posts.data.allMarkdownRemark.edges.map((post) => {
    return(
      <div key={post.node.frontmatter.slug}>
        <h2 className="fw3">{post.node.frontmatter.title}</h2>
        <p>{post.node.excerpt}</p>
        <hr/>
        <strong>by </strong>{post.node.frontmatter.author}
        &nbsp;|&nbsp;
        <strong>on </strong>{post.node.frontmatter.date}
        &nbsp;|&nbsp;
        <Link to={"/" + post.node.frontmatter.slug}>read</Link>
        <hr/>
      </div>
    )
  })
}

const Index = ({data}) => {
  return (
    <Layout className="pa3">
        <h1 className="f-subheadline fw9 tracked-tight tc">Welcome to my blog</h1>
        <div className="blog-posts">
          {listPosts({data})}
        </div>
      </Layout>
  );
}
 
export default Index;